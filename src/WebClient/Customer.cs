namespace WebClient
{
    public class Customer
    {
        public long Id { get; set; }
        public string Firstname { get; init; }
        public string Lastname { get; init; }
    }
}