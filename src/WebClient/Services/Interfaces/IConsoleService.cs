﻿using System.Threading.Tasks;

namespace WebClient.Services.Interfaces
{
    public interface IConsoleService
    {
        public Task Start();
    }
}