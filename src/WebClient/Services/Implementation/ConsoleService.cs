﻿using System;
using System.Threading.Tasks;
using WebClient.Services.Interfaces;

namespace WebClient.Services.Implementation
{
    public class ConsoleService : IConsoleService
    {
        private bool _isBusy = true;
        private readonly IWebApiService _wepApiService;
        private readonly IGeneratorService _generatorService;
        public ConsoleService(IWebApiService service, IGeneratorService generator)
        {
            _wepApiService = service;
            _generatorService = generator;
        }

        public async Task Start()
        {
            await _wepApiService.CreateCustomer(_generatorService.Generate());
            await _wepApiService.CreateCustomer(_generatorService.Generate());
            await _wepApiService.CreateCustomer(_generatorService.Generate());
            await _wepApiService.CreateCustomer(_generatorService.Generate());
            await _wepApiService.CreateCustomer(_generatorService.Generate());
            
            do
            {
                var customersCount = await _wepApiService.GetAmount();
                Console.WriteLine($"Добро пожаловать, сейчас на сервере доступно {customersCount} клиентов, выберите, что хотите сделать \n" +
                                  $"1 - вывести всех клиентов \n" +
                                  $"2 - вывести клиента с заданным id \n" +
                                  $"3 - сгенерировать клиента \n" +
                                  $"4 - перегенирировать клиента с заданным id \n" +
                                  $"5 - удалить клиента с заданным id \n" +
                                  $"0 - для выхода из приложения");

                if (!int.TryParse(Console.ReadLine(), out var nextStep))
                {
                    Console.WriteLine("Введено не число, попробуйте еще раз");
                    Console.Clear();
                    continue;
                }

                switch (nextStep)
                {
                    case 1:
                        await ShowCustomers();
                        break;
                    case 2:
                        await ShowSpecificCustomer();
                        break;
                    case 3:
                        await CreateCustomer();
                        break;
                    case 4:
                        await UpdateCustomer();
                        break;
                    case 5:
                        await DeleteCustomer();
                        break;
                    case 0:
                        _isBusy = false;
                        continue;
                    default:
                        Console.Clear();
                        Console.WriteLine("Команда не найдена. Попробуйте еще раз");
                        continue;
                }
            } while (_isBusy);
        }

        private async Task UpdateCustomer()
        {
            Console.Clear();
            Console.WriteLine("Введеное id клиента для перегенирирования");

            var incorrectValue = true;

            do
            {
                if (!long.TryParse(Console.ReadLine(), out var id))
                {
                    Console.WriteLine("Введеное число не является числом");
                    continue;
                }
                incorrectValue = false;

                var customer = _generatorService.Generate();
                customer.Id = id;
                var customerId = await _wepApiService.UpdateCustomer(id, customer);

                if (customerId == -1)
                {
                    Console.WriteLine("Некорректный запрос");
                }
                else
                {
                    Console.WriteLine("Перегенирированый клиент");
                    await ShowSpecificCustomer(customerId);
                }
            } while (incorrectValue);

            Console.WriteLine("Для выхода в меню нажмите любую кнопку");
            Console.ReadLine();
            Console.Clear();
        }

        private async Task DeleteCustomer()
        {
            Console.Clear();
            Console.WriteLine("Введеное id клиента для удаления");

            var incorrectValue = true;

            do
            {
                if (!long.TryParse(Console.ReadLine(), out var id))
                {
                    Console.WriteLine("Введеное число не является числом");
                    continue;
                }

                incorrectValue = false;
                await _wepApiService.DeleteCustomer(id);
                Console.WriteLine("Пользователь успешно удален");
            } while (incorrectValue);

            Console.WriteLine("Для выхода в меню нажмите любую кнопку");
            Console.ReadLine();
            Console.Clear();
        }

        private async Task CreateCustomer()
        {
            Console.Clear();
            Console.WriteLine("Сгенерированный клиент");

            var customer = _generatorService.Generate();
            var customerId = await _wepApiService.CreateCustomer(customer);

            await ShowSpecificCustomer(customerId);

            Console.WriteLine("Для выхода в меню нажмите любую кнопку");
            Console.ReadLine();
            Console.Clear();
        }

        private async Task ShowSpecificCustomer(long id)
        {
            var customer = await _wepApiService.GetCustomer(id);
            Console.WriteLine(customer.Id + " " + customer.Firstname + " " + customer.Lastname);
        }

        private async Task ShowSpecificCustomer()
        {
            Console.Clear();
            Console.WriteLine("Введеное id клиента");

            var incorrectValue = true;

            do
            {
                if (!long.TryParse(Console.ReadLine(), out var id))
                {
                    Console.WriteLine("Введеное число не является числом");
                    continue;
                }

                incorrectValue = false;
                var customer = await _wepApiService.GetCustomer(id);

                if (customer != null)
                    Console.WriteLine(customer.Id + " " + customer.Firstname + " " + customer.Lastname);
                else
                    Console.WriteLine("Пользователь не найден");

            } while (incorrectValue);

            Console.WriteLine("Для выхода в меню нажмите любую кнопку");
            Console.ReadLine();
            Console.Clear();
        }

        private async Task ShowCustomers()
        {
            Console.Clear();
            var customersArray = await _wepApiService.GetCustomers();

            foreach (var customer in customersArray)
            {
                Console.WriteLine(customer.Id + " " + customer.Firstname + " " + customer.Lastname);
            }

            Console.WriteLine("Для выхода в меню нажмите любую кнопку");
            Console.ReadLine();
            Console.Clear();
        }
    }

}