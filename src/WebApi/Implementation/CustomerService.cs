﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Interfaces;
using WebApi.Models;

namespace WebApi.Implementation
{
    public class CustomerService : ICustomerService
    {
        private readonly Dictionary<long, Customer> _customerDictionary;

        public CustomerService()
        {
            _customerDictionary = new Dictionary<long, Customer>();
        }

        public Task<Customer[]> GetAllAsync()
        {
            return Task.FromResult(_customerDictionary.Values.ToArray());
        }

        public Task<long> GetAmountAsync()
        {
            return Task.FromResult((long)_customerDictionary.Count);
        }

        public Task<Customer> GetAsync(long id)
        {
            return Task.FromResult(_customerDictionary.TryGetValue(id, out var customer) ? customer : null);
        }

        public Task<bool> AddAsync(Customer customer)
        {
            if (_customerDictionary.TryGetValue(customer.Id, out _))
                customer.Id = _customerDictionary.Select(x => x.Key).Max() + 1;

            return Task.FromResult(_customerDictionary.TryAdd(customer.Id, customer));
        }

        public Task<bool> UpdateAsync(long id, Customer customer)
        {
            if (!_customerDictionary.TryGetValue(id, out _))
                return Task.FromResult(false);

            _customerDictionary.Remove(id);
            _customerDictionary.Add(customer.Id, customer);

            return Task.FromResult(true);
        }

        public Task DeleteAsync(long id)
        {
            if (_customerDictionary.TryGetValue(id, out _))
                _customerDictionary.Remove(id);

            return Task.CompletedTask;
        }
    }
}