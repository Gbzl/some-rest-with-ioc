﻿using System.Linq;
using PersonGenerator;
using WebClient.Services.Interfaces;

namespace WebClient.Services.Implementation
{
    public class GeneratorService : IGeneratorService
    {
        public Customer Generate()
        {
            var settings = new GeneratorSettings
            {
                Language = Languages.English,
                FirstName = true,
                LastName = true
            };
            var personGenerator = new PersonGenerator.PersonGenerator(settings).Generate(1).FirstOrDefault();

            return new Customer { Firstname = personGenerator.FirstName, Lastname = personGenerator.LastName };
        }
    }

}