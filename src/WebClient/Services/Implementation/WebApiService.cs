﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WebClient.Services.Interfaces;

namespace WebClient.Services.Implementation
{
    public class WebApiService : IWebApiService
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public WebApiService(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<Customer> GetCustomer(long id)
        {
            var client = _httpClientFactory.CreateClient("web-api");

            using var response = await client.GetAsync($"{id}");
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Customer>(content);
            }
            else
            {
                return null;
            }
        }

        public async Task<long> GetAmount()
        {
            var client = _httpClientFactory.CreateClient("web-api");

            using var response = await client.GetAsync($"amount");
            var content = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<long>(content);
        }

        public async Task<Customer[]> GetCustomers()
        {
            var client = _httpClientFactory.CreateClient("web-api");

            using var response = await client.GetAsync(client.BaseAddress);
            var content = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<Customer[]>(content);
        }

        public async Task<long> CreateCustomer(Customer customer)
        {
            var client = _httpClientFactory.CreateClient("web-api");
            var customerRequest = new CustomerCreateRequest(customer.Id, customer.Firstname, customer.Lastname);

            var content = new StringContent(JsonConvert.SerializeObject(customerRequest), Encoding.UTF8);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync(client.BaseAddress, content);

            return long.Parse(await response.Content.ReadAsStringAsync());
        }

        public async Task DeleteCustomer(long id)
        {
            var client = _httpClientFactory.CreateClient("web-api");

            await client.DeleteAsync($"{id}");
        }

        public async Task<long> UpdateCustomer(long id, Customer customer)
        {
            var client = _httpClientFactory.CreateClient("web-api");
            var customerRequest = new CustomerCreateRequest(customer.Id, customer.Firstname, customer.Lastname);

            var content = new StringContent(JsonConvert.SerializeObject(customerRequest), Encoding.UTF8);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = await client.PutAsync($"{id}", content);
            if (response.IsSuccessStatusCode)
                return long.Parse(await response.Content.ReadAsStringAsync());
            else
                return -1;
        }       
    }

}