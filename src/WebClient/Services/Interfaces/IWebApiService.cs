﻿using System.Threading.Tasks;

namespace WebClient.Services.Interfaces
{
    public interface IWebApiService
    {
        Task<Customer[]> GetCustomers();
        Task<long> GetAmount();
        Task<Customer> GetCustomer(long id);
        Task<long> CreateCustomer(Customer customer);
        Task DeleteCustomer(long id);
        Task<long> UpdateCustomer(long id, Customer customer);
    }
}