using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Interfaces;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        private readonly ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpGet("")]
        public async Task<ActionResult<Customer[]>> GetAllAsync()
        {
            return Ok(await _customerService.GetAllAsync());
        }

        [HttpGet("{id:long}")]   
        public async Task<ActionResult<Customer>> GetCustomerAsync([FromRoute] long id)
        {
            var customer = await _customerService.GetAsync(id);

            if (customer == null)
                return NotFound();

            return Ok(customer);
        }

        [HttpGet("amount")]
        public async Task<ActionResult<long>> GetCustomersAmountAsync()
        {
            return Ok(await _customerService.GetAmountAsync());
        }

        [HttpPost("")]
        public async Task<ActionResult<long>> CreateCustomerAsync([FromBody] Customer customer)
        {
            if (!await _customerService.AddAsync(customer))
                return BadRequest();

            return Ok(customer.Id);
        }

        [HttpPut("{id:long}")]
        public async Task<ActionResult<long>> UpdateCustomerAsync([FromRoute] long id, [FromBody] Customer customer)
        {
            if (id != customer.Id)
                return BadRequest();

            if (!await _customerService.UpdateAsync(id, customer))
               return NotFound();

           return Ok(customer.Id);
        }

        [HttpDelete("{id:long}")]
        public async Task<ActionResult> DeleteCustomerAsync([FromRoute] long id)
        {
            await _customerService.DeleteAsync(id);

            return Ok();
        }
    }
}