﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using WebClient.Services.Implementation;
using WebClient.Services.Interfaces;

namespace WebClient
{
    static class Program
    {
        static async Task Main(string[] args)
        {
            var provider = Configure();
            var console = provider.GetService<IConsoleService>();
            await console.Start();
        }

        private static IServiceProvider Configure()
        {
            var services = new ServiceCollection();
            services.AddHttpClient("web-api", client =>
            {
                client.BaseAddress = new Uri("https://localhost:5001/customers/");
            });
            services.AddScoped<IWebApiService, WebApiService>();
            services.AddScoped<IConsoleService, ConsoleService>();
            services.AddTransient<IGeneratorService, GeneratorService>();

            return services.BuildServiceProvider();
        }

    }
}