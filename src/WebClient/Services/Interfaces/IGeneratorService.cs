﻿namespace WebClient.Services.Interfaces
{
    public interface IGeneratorService
    {
        Customer Generate();
    }
}