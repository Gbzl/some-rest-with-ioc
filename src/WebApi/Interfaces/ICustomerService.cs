﻿using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.Interfaces
{
    public interface ICustomerService
    {
        Task<Customer[]> GetAllAsync();
        Task<long> GetAmountAsync();
        Task<Customer> GetAsync(long id);
        Task<bool> UpdateAsync(long id, Customer customer);
        Task<bool> AddAsync(Customer customer);
        Task DeleteAsync(long id);
    }
}